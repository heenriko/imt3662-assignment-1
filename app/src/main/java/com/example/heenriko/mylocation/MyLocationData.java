package com.example.heenriko.mylocation;

/**
 * Created by heenriko on 01.09.2015.
 */


import android.location.Address;
import android.location.Geocoder;
import android.content.Context;
import android.util.Log;


import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * This is my own location storage of the current location.
 * The object is created in mapsActivity.java where the
 * longitude and latitude is passed as arguments.
 * By those values there is possible to get the city name etc.
 *
 * The purpose is to access to the city name and saving the location
 * to the database.
 */


public class MyLocationData {

    // Properties:

    private String  city;
    private int     temperature;
    private float   seaHeight;
    private double  longitude;
    private double  latitude;
    private int     cameraMapZoom;
    private static final String TAG = "Location class Says: ";

    DatabaseHandler db;

    // Constructor:
    // The context is the mapsActivity, the purpose of this is to get the city name.

    public MyLocationData(Context context, double latitude, double longitude, int cameraMapZoom) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.cameraMapZoom = cameraMapZoom;
        this.city = this.setCity(context, latitude, longitude);

        db = new DatabaseHandler(context, null, null, 1);
        db.saveLocation(this);

        Log.i(TAG, "City: " + this.city  );
    }


    // Getters:

    public String getCity() {
        return city;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public int getCameraMapZoom() {
        return cameraMapZoom;
    }


    // Setters:

    /**
     * Got the code from this url:
     * http://stackoverflow.com/questions/2296377/how-to-get-city-name-from-latitude-and-longitude-coordinates-in-google-maps
     *
     * The function returns the city name, if available.
     * Get city name from longitude and latitude.
     */

    public String setCity(Context context, double latitude, double longitude) {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());

        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return addresses != null ? addresses.get(0).getLocality() : "Fetching error";
    }

}