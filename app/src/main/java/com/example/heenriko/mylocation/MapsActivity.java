package com.example.heenriko.mylocation;

import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * This is the map activity where the user can see
 * his/hers current loacation. The main purpose here
 * is to get longitude (lat) and latitude from the current
 * location and set a marker on the map.
 *
 * I get the lat and lng from the class Location, and then I create
 * my own object of class MyLocationData, where i pass these values
 * to the constructor method, along with the desired camera zoom.
 * See MylocationData.java for more information.
 *
 * I got the code from Ben Jakuben's tutorial:
 * URL: http://blog.teamtreehouse.com/beginners-guide-location-android
 *
 * The tutorial describes how to update the current
 * location for each new location. I've partially
 * used this code so you only get the location
 * by calling the function from OnConnected.
 *
 */


public class MapsActivity extends FragmentActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    private GoogleApiClient mGoogleApiClient;

    private static final String TAG = "Google Maps:";

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.


    // Declare variables:
    private TextView cityText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Assign ID to variables:
        cityText = (TextView) findViewById(R.id.city);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            Log.i(TAG, "Location returned null");
            cityText.setText("You're not connected");
        } else {
            handleNewLocation(location);
        };
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        cityText.setText("Connection failed");
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    private void handleNewLocation(Location location) {

        // New objLocation to store location data, pass this(mapsActivity context)
        MyLocationData objLocation = new MyLocationData
                (this, location.getLatitude(), location.getLongitude(), 8);

        // Create a LatLng object for the current location
        LatLng latLng = new LatLng(objLocation.getLatitude(), objLocation.getLongitude());


        // Set marker:
        mMap.addMarker(new MarkerOptions().position
                (new LatLng(objLocation.getLatitude(), objLocation.getLongitude())));

        // Show the current location in Google Map
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        // Zoom in the Google Map.
        mMap.animateCamera(CameraUpdateFactory.zoomTo(objLocation.getCameraMapZoom()));

        // Display the city name:
        cityText.setText(objLocation.getCity());
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */

    private void setUpMap() {

        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }
}
