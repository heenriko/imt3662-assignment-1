package com.example.heenriko.mylocation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.content.Intent;

/**
 * This is the front page where the user can do following actions:
 *  - Go to maps activity to check your current location.
 *  - Display the database log to see your previous locations(city).
 *  - Clear the entire rows in the database.
 *
 */


public class FrontPageActivity extends AppCompatActivity {

    // Declare variables:
    Button getLocationBtn;
    Button displayLogBtn;
    Button clearDbBtn;
    TextView dbLog;

    // Initialize classes:
    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);

        db = new DatabaseHandler(this, null, null, 1);

        // Assign id's to variables:
        getLocationBtn = (Button)findViewById(R.id.getLocationButton);
        displayLogBtn = (Button)findViewById(R.id.displayLogButton);
        clearDbBtn = (Button)findViewById(R.id.clearDbButton);
        dbLog = (TextView)findViewById(R.id.dbLog);


        // Go to Map activity:
        getLocationBtn.setOnClickListener(
            new Button.OnClickListener(){
                public void onClick(View v){
                    startActivity(new Intent(FrontPageActivity.this, MapsActivity.class));
                }
            }
        );

        // Display database log:
        displayLogBtn.setOnClickListener(
            new Button.OnClickListener() {
                public void onClick(View v) {
                    printDBLog();
                }
            }
        );

        // Clear database:
        clearDbBtn.setOnClickListener(
            new Button.OnClickListener() {
                public void onClick(View v) {
                    clearDatabase();
                }
            }
        );
    }

    // Print the database log:
    protected void printDBLog(){
        // Get database in string format:
        String dbString = db.databaseToString();

        // Display database log if not empty:
        if(dbString != "")
            dbLog.setText(dbString);
        else
            dbLog.setText("Empty Database");


    }
    // Clear all rows in the database:
    protected void clearDatabase(){
        // Clear the database:
        db.clearDB();
        // Print the database log to show empty database:
        printDBLog();

    }

}
