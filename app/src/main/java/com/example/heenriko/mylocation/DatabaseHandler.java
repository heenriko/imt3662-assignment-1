package com.example.heenriko.mylocation;

/**
 * Created by heenriko on 09.09.2015.
 */

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.util.Log;

/**
 * This is the database handler file.
 * Creates a new database file: locations.db
 * Table: locations.
 * Columns: ID, CITY and TIMESTAMP.
 *
 * The possible actions are:
 *      Saving City and Timestamp,
 *      Clear all table rows,
 *      Convert all rows to string format (ready to print).
 *
 * I got the code from theNewBoston's tutorial:
 * Youtube Tutorial (49-54) URL: https://www.youtube.com/playlist?list=PL6gx4Cwl9DGBsvRxJJOzG4r4k_zLKrnxl
 * Source Code:             URL: https://www.thenewboston.com/forum/topic.php?id=3767
 *
 * I have altered the variables and the methods to my purpose of the application.
 *
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "locationDB.db";
    public static final String TABLE_LOCATION = "locations";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_TIME = "timeStamp";

    private static final String TAG = "Database says: ";

    //We need to pass database information along to superclass
    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_LOCATION + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_CITY + " VARCHAR(32), " +
                COLUMN_TIME + " DATETIME DEFAULT CURRENT_TIMESTAMP " +
                ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);
        onCreate(db);
    }

    // Add a new row to the database
    public void saveLocation(MyLocationData location){
        ContentValues values = new ContentValues();
        values.put(COLUMN_CITY, location.getCity());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_LOCATION, null, values);
        db.close();
    }

    // Clear all rows in table locations:
    public void clearDB(){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_LOCATION, null, null);
        db.close();
    }

    // Converts all rows to string format:
    public String databaseToString(){
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * " + "FROM " + TABLE_LOCATION + " WHERE 1 ORDER BY timeStamp DESC LIMIT 5";

        //Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);

        //Move to the first row in your results
        c.moveToFirst();

        //Position after the last row means the end of the results
        while (!c.isAfterLast()) {
            if ( c.getString(c.getColumnIndex("city")) != null) {
                dbString += "You were in ";
                dbString += c.getString(c.getColumnIndex("city"));
                dbString += " at ";
                dbString += c.getString(c.getColumnIndex("timeStamp"));
                dbString += "\n\n";
            }
            c.moveToNext(); // Go to next row.
        }

        db.close();

        Log.i(TAG, dbString);

        return dbString;
    }

}